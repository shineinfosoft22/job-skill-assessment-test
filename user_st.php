<?php

session_start();
 
if(isset($_POST['save'])){

    $_SESSION['userid'] = $_POST['userid'];
    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['lastname'] = $_POST['lastname'];
    $_SESSION['email'] = $_POST['email'];
    // $_SESSION['profile'] = $_FILES['profile'];
    $filename    = $_FILES["profile"]['name'];
    $type = $_FILES["profile"]['type'];

    $destination = "upload/" . $_FILES["profile"]["name"]; 
    move_uploaded_file($filename, $destination); 

    $_SESSION['profile'] = $filename;

    // print_r($destination);

    $newURL = 'user.php';
    header('Location: '.$newURL);
}
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>user</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <style>
  .container {
    max-width: 500px;
    margin: 50px auto;
    text-align: left;
    font-family: sans-serif;
}
form {
    border: 1px solid #1A33FF;
    background: #ecf5fc;
    padding: 40px 50px 45px;
}
.form-control:focus {
    border-color: #000;
    box-shadow: none;
}
label {
    font-weight: 600;
}
.error {
    color: red;
    font-weight: 400;
    display: block;
    padding: 6px 0;
    font-size: 14px;
}
.form-control.error {
    border-color: red;
    padding: .375rem .75rem;
}
</style>
</head>
<body>
  <div class="container mt-5">
    <!-- user form -->
    <form action="" name="userForm" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <button>Student</button>
        <button>Teacher</button>
        <button>Parent</button>
    </div>
      <div class="form-group">
        <label>User ID</label>
        <input type="text" class="form-control" name="userid" id="userid">
      </div>
      <div class="form-group">
        <label>First Name</label>
        <input type="text" class="form-control" name="firstname" id="firstname">
      </div>
      <div class="form-group">
        <label>Last Name</label>
        <input type="text" class="form-control" name="lastname" id="lastname">
      </div>
      <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email" id="email">
      </div>
      <div class="form-group">
        <label>Profile</label>
        <input type="file" class="form-control" name="profile" id="profile">
      </div>
      <input type="submit" name="save" value="save" class="btn btn-dark btn-block">
    </form>
  </div>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>

<script>
    
    $(function () {
        $("form[name='userForm']").validate({
            // Define validation rules
            rules: {
                userid: {
                    required: true,
                    number: true
                },
                firstname: {
                    required: true,
                },
                lastname: {
                    required: true,
                },
                email: {
                    required: true,
                    email:true
                },
                profile: {
                    required:true,
                    extension:jpg,
                },
            },
            messages: {
                userid: {
                    required:"This user id fild required",
                    number:"Please enter number"
                },
                firstname: "This firstname fild is required",
                lastname: "This firstname fild is required",
                email: {
                    required: "Please enter your email",
                    minlength: "Please enter a valid email address"
                },
                profile: {
                    required:"Image required",
				extension: "You're only allowed to upload jpg or png images."
			},
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }); 
</script>
</body>

</html>