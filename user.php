<?php

session_start();
 

    $_POST['userid'] = $_SESSION['userid'];
    $_POST['firstname'] = $_SESSION['firstname'];
    $_POST['lastname'] = $_SESSION['lastname'];
    $_POST['email'] = $_SESSION['email'];
    $filename = $_SESSION['profile'];
   

?><!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>user</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <style>
  .container {
    max-width: 500px;
    margin: 50px auto;
    text-align: left;
    font-family: sans-serif;
}
form {
    border: 1px solid #1A33FF;
    background: #ecf5fc;
    padding: 40px 50px 45px;
}
.form-control:focus {
    border-color: #000;
    box-shadow: none;
}
label {
    font-weight: 600;
}
.error {
    color: red;
    font-weight: 400;
    display: block;
    padding: 6px 0;
    font-size: 14px;
}
.form-control.error {
    border-color: red;
    padding: .375rem .75rem;
}
</style>
</head>
<body>
  <div class="container mt-5">
      <div class="form-group">
        <label>User ID</label>
        <input type="text" class="form-control" name="userid" id="userid" value="<?php echo $_POST['userid'] ?>">
      </div>
      <div class="form-group">
        <label>First Name</label>
        <input type="text" class="form-control" name="firstname" id="firstname" value="<?php echo $_POST['firstname'] ?>">
      </div>
      <div class="form-group">
        <label>Last Name</label>
        <input type="text" class="form-control" name="lastname" id="lastname" value="<?php echo $_POST['lastname'] ?>">
      </div>
      <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email" id="email" value="<?php echo $_POST['email'] ?>">
      </div>
      <div class="form-group">
        <label>Profile</label>
        <input type="text" class="form-control" name="profile" id="profile" value="<?php echo $filename ?>">
      </div>
  </div>
</body>
</html>